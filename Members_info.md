# Contact list

## Email
Bruno Postle <bruno@postle.net>

Dion Moult <dion@thinkmoult.com>

Duncan Lithgow <duncan@lithgow-schmidt.dk>

Peter (cadgiru) <cadgiru@gmail.com>

Ioannis Christovasilis (Jesusbill) <ipc@aethereng.com>

Ryan Schultz (theoryshaw) <ryan@openingdesign.com>

# Major City (for timezone planning)
Bruno Postle: London, England

Dion Moult: Sydney, Australia

Duncan Lithgow: Copenhagen, Denmark

Peter (cadgiru): Oslo, Norway

Ioannis Christovasilis (Jesusbill): Rome, Italy

Ryan Schultz (theoryshaw): Chicago, USA
