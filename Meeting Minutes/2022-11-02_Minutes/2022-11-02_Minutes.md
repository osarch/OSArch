Date: 2022-11-02
Participants

- Peter Sande
- Ioannis Christovasilis
- Ryan Schultz
- Yassine Oualid

### Minutes

1. Discussed the next stage for [A Call for Projects to Fund via OSArch's Open Collective Site](https://community.osarch.org/discussion/1156/a-call-for-projects-to-fund-via-osarchs-open-collective-site#latest)
   - Decided to transfer all the proposals over to the 2nd voting thread and let the community vote.
   - In this 2nd voting thread, we will remind the community to vote for a project that has a tight scope.
   - In the future iterations for these 'call for proposals' campaigns we can delineate (and police) tighter requirements, as discussed in Duncan [thread](https://community.osarch.org/discussion/1203/what-is-a-good-submission-for-funding-and-do-we-have-any#latest). Since, however, we did not for this 1st iteration, and to prevent alienating anyone, we decided to push all the proposals through to this 2nd voting thread, and let the community vote.
1.  Yass asked the question and suggested that OSArch allow anyone to make a proposal on a platform like Open Collective. It appeared there was some agreement that this was a good idea, but nothing was decided. Could be a discussion topic for the next SC meeting.
1. Chatted a little bit about paying for our technology infrastructure (fees for website, wiki, forum, etc.) from our Open Collective site. General consensus seemed to indicate that was a good idea.
1. Yass further shared his idea of creating a fundraising campaign around paid tutorials around Python, as it applies to BlenderBIM/IfcOpenShell, as well as perhaps user tutorials.
   - Seemed unanimous that the SC thought it was a good idea.
   - Yass to follow up with a more refined proposal
   - Ioannis showed interested in helping
   - Ryan could help with BB user tutorials.
1. Discussed starting up the monthly meetups/presentations again.
   - SC agreed it was a good idea.
   - To lighten the amount of prep time and overhead these meetings require, Ryan suggested that perhaps every other month it was a more informal roundtable chat, with somewhat of an open agenda.
     - General consensus seemed to indicate this was a good idea.
   - Ioannis and Yass said that they could collaborate on the organizational part.
1. As a suggestion on what we could spend our general OC funding on, Peter suggested some type of initiative to summarize and distill all the knowledge that is shared on the forum.
   - The wiki was suggested as the location this could happen, but no other specifics were discussed.
