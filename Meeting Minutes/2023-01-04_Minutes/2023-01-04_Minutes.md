# Suggested agenda
   * Possible agenda topics can be added to Etherpad https://scalar.vector.im/etherpad/p/!TXapfsdaYQHivXBBEN_matrix.org

# Meeting notes for 2023-01-04

## Fixed agenda
   1. Who chairs the meeting? Duncan
   1. Who takes minutes? Duncan
   1. Follow up on previous decisions: see agenda
   1. Decide agenda
   1. Present were Bruno, Peter, Ioannis & Duncan

## Quick topics
   * No update on courses from Yassine
   * No update on OSArch Gitea. Duncan was unhappy about using AWS and suggests looking at a hosted solution to lower our workload. 

## Main Topics

### Bruno's IFC GIT interface.
   - next steps: 
      Dedicated post on the forum outlining the types of development projects that could be relevant. Task for Bruno. "OSArch funded project: IFC GIT ..." Devs. can then contribute to the discussion and suggest what they'd like to work on with the funds.
      - structure for receiving funds: target USD500 adding maks USD500 from general fund if the goal is met.
      - Target USD500 in the first month, ready to extend the deadline. LinkedIn, forum, Twitter, Masstodon, Newsletter ... SoMe focuses on funding (task for Duncan), forum & newsletter on the details (another task for Bruno, estimated draft ready inside two weeks)

### Process
   - Do we need a Code of Conduct? Easy to copy/paste, maybe also from https://wiki.osarch.org/index.php?title=Organization
   - Do we need a Committee Procedure document? (Duncan mistakenly called this an 'Order of Business' which is just an agenda) There must be examples. Duncan has earlier collected some resources on this page, mostly named organizations that are similar to us: https://wiki.osarch.org/index.php?title=Organization also found these: https://duckduckgo.com/?q=committee+procedures+and+practices+open+source&t=ffab&ia=web & wikipedia: https://en.wikipedia.org/wiki/Parliamentary_procedure
   - Do we need a Charter / Mission Statement? Here is an earlier Mission Statement thread: https://community.osarch.org/discussion/59/osarch-mission-statement/p1 & an OSArch Vision thread: https://community.osarch.org/discussion/168/vision-for-osarch/
   Earlier decison was: "Making decisions when (2) people disagree and no else chimes in.  How to avoid it going stall?" Todays discussion highlighted that we need to define the types of decisions that need a discussion period before a binding decision. Such a process would be described in an order of business
(here is the forum thread where some of the chat about decision making continued: https://community.osarch.org/discussion/1291/decision-making-in-the-steering-committee#latest )

# Possible future agenda items
   * Yass's paid tutorial website
   * OSarch run Gitea? - where?

# Host/organiser rotation list:
   - January: Duncan
   - February: Jesusbill
   - March: Moult
   - April: Ryan
   - May: Peter
   - June: Bruno

Host responsibility
    * Ask for & suggest agenda items
    * Check previous agenda for issues that need follow up
    * Make sure someone takes notes
    * Make sure we check all the agenda topics
