#### Title: Voting for 'the' project to be hosted on OSArch's Open Collective Site

First off, thank you to everyone that proposed a project on the [call for proposals](https://community.osarch.org/discussion/1156/) thread. 

As mentioned there, this is the new thread where the community can vote on which project we should host on our [Open Collective](https://opencollective.com/osarch) site.

The proposals below are taken from the original [thread](https://community.osarch.org/discussion/1156/). If needed we will work with the most popular proposal to refine the funding projects scope.

---

To vote, please 'like' ![enter image description here](https://www.dropbox.com/s/hkbbkhvofyzp5fi/2022-11-04_10-02-51_community.osarch.org_discussion1230osarch-website-_chrome.png?dl=1) up to three projects.

We will keep this voting open for the rest of November. 

By voting for three projects, we get a sort of [ranked voting](https://en.wikipedia.org/wiki/Ranked_voting) which makes it easier for us to see the community's priorities.

---

Again, if you have an idea on how to improve this process of proposing and selecting a project, please suggest your ideas [here](https://community.osarch.org/discussion/1157/improving-the-process-of-selecting-projects-to-post-on-our-open-collective-site/). 

Thank You on behalf of OSArch's Steering Commmitee!
Dion (@moult), Ioannis (@jesusbill), Peter (@cadgiru), Bruno (@brunopostle), Duncan (@duncan) & Ryan (@theoryshaw)

---

Twitter/Linkedin/etc...

Please drop by and vote for the project you think should be hosted on our Open Collective site!
https://community.osarch.org/discussion/xxx

