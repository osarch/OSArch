### Have an idea you'd like to get funding for?

The OSArch Committee has decided to let you, the OSarch community, choose which projects you'd like to see posted on our [OpenCollective site](https://opencollective.com/osarch) account.

And for every dollar that your project pulls in, OSArch will match it with their general funds.   *That is of course, if we have enough general funds to cover it!* :) 

### Who can create funding projects?

You can!... if you have 100 or more reputation points on your OSArch profile page.  You can see how many points you have, on your profile page.  

 - To find your profile, use the following url pattern: https://community.osarch.org/profile/<YourUserNameHere\>
 - Your points are located here, for example...
  <br>![](points.png)


### Where do I post the project

Unfortunately, OpenCollective does not have a variety of permission roles.  There's basically only an ***admin*** role that can do everything, including deleting our entire OC account, if they wanted.  So in the vein of being safe, the steering committee (SC) will post your project on our OC account.

To create a funding project, just [create a new forum post](https://community.osarch.org/post/discussion), and ping us here.  We will essentially copy/paste your project blurb (and images) to our Open Collective site.

###  What do I include in the project write up? 

Anything that you think could sell the project to prospective funders and prospective solution providers. 

The key aspect, however, is that the project should have a **tight, atomized scope of work**. A tight delineated scope will allow us to determine when the project is done, and when the funds should be distributed to the author(s).


###  Who will determine when a project is done?

The OSArch steering committee will be the ultimate arbiter of when a project is done. 

Anyone familiar with software development knows that it's sometimes hard, if not always hard, to determine when exactly the project is complete.   Even when the scope is clearly defined, quite often there's a sense that more can be done to refine the project.  This is unavoidable, so in that light, to be fair to both the funders and solution providers, the SC will act as the arbiter to determine when the project is done and when the funds should be distributed.  When it's 'done' will also be a factor of how much the funding is relative to how involved the scope is.  That is say, if a very small amount of funding is provided, for a solution that would require a lot more funding, the SC can allocate funds for partial solutions, as well.

Please note, if no one provides a solution to the project with a year (365days), the funds dedicated to this project will be transferred over to OSarch's [general pool of funds](https://opencollective.com/osarch#category-BUDGET_).

...

If you have any questions, please ask them here.  We're update the above blurb with an answers that may result. 

Best, Ryan